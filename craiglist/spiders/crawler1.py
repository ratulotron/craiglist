from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from craiglist.items import CraiglistItem


class MySpider(BaseSpider):
    name = "craigi"
    allowed_domains = ["craigslist.org"]
    start_urls = ["http://sfbay.craigslist.org/npo/"]

    def parse(self, response):
        hxs = HtmlXPathSelector(response)
        # selects the spans with class "pl"
        titles = hxs.select("(//span[@class='pl'])[1]")
        # blank list to hold
        items = []
        item = CraiglistItem()
        item["title"] = titles.select("a/text()").extract()
        item["link"] = titles.select("a/@href").extract()
        items.append(item)
        # for title in titles:
        #     item = CraiglistItem()
        #     item["title"] = title.select("a/text()").extract()
        #     item["link"] = title.select("a/@href").extract()
        #     items.append(item)
        return items
